//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class Contato: Encodable {
    var nome: String
    var email: String
    var endereco: String
    var telefone: String
    
    init(nome: String, email: String, endereco: String, telefone: String) {
        self.nome = nome
        self.email = email
        self.endereco = endereco
        self.telefone = telefone
    }
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    var userDefaults = UserDefaults.standard
    let listaDeContatosKey  = " ListaDeContatos"
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        celula.nome.text = contato.nome
        celula.email.text = contato.email
        celula.endereco.text = contato.endereco
        celula.telefone.text = contato.telefone
        
        return celula
    }
    

    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        tableview.delegate = self
        
        listaDeContatos.append(Contato(nome: "Contato 1", email: "contato1@gmail.com", endereco: "Rua do Endereco, 1",telefone: "00000-0000"))
        listaDeContatos.append(Contato(nome: "Contato 2", email: "contato2@gmail.com", endereco: "Avenida do Endereco, 2",telefone: "11111-1111"))
        listaDeContatos.append(Contato(nome: "Contato 3", email: "contato3@gmail.com", endereco: "Alameda do Endereco, 3",telefone: "22222-2222"))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "abrirDetalhes" {
            let detalhesViewController = segue.destination as! DetalhesViewController
            let index = sender as! Int
            let contato = listaDeContatos[index]
            detalhesViewController.index = index
            detalhesViewController.contato = contato
            detalhesViewController.delegate = self
            detalhesViewController.contatoDelegate = self
        } else if segue.identifier == "criarContato" {
            let contatoViewController = segue.destination as! ContatoViewController
            contatoViewController.delegate = self
        }
    }
}

extension ViewController: DetalhesViewControllerDelegate {
    func excluirContato(index: Int) {
        listaDeContatos.remove(at: index)
        tableview.reloadData()
        salvarContatosLocal()
        
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "abrirDetalhes", sender: indexPath.row)
    }
}

extension ViewController: ContatoViewControllerDelegate {
    func salvarContatosLocal(){
    do{
        let dataLista = try JSONEncoder().encode(listaDeContatos)
        userDefaults.setValue(dataLista, forKey: listaDeContatosKey)
    } catch{
        print ("Erro ao salvar dados na memoria local: \(error.localizedDescription)")
    }
}

    
    func salvarNovoContato(contato: Contato) {
        listaDeContatos.append(contato)
        tableview.reloadData()
        salvarContatosLocal()
    }
       
    
    func editarContato() {
        tableview.reloadData()
        salvarContatosLocal()
        
        
    }   
}
